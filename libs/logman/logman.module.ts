import {  Module } from "@nestjs/common";
import { LogmanService } from "./logman.service";

@Module({
  providers: [LogmanService],
  exports: [LogmanService],
})
export class LogmanModule {
  
}



