"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ScreaManModule = exports.LogmanModule = void 0;
var logman_module_1 = require("./logman/logman.module");
Object.defineProperty(exports, "LogmanModule", { enumerable: true, get: function () { return logman_module_1.LogmanModule; } });
var screamMan_module_1 = require("./screamMan/screamMan.module");
Object.defineProperty(exports, "ScreaManModule", { enumerable: true, get: function () { return screamMan_module_1.ScreaManModule; } });
//# sourceMappingURL=index.js.map